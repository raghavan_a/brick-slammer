This is the repository for my Android game 'Brick Slammer' which can be downloaded from https://play.google.com/store/apps/details?id=com.myandroid.brickslammer

About the repo: 

The 'BrickSlammer' package contains code intended to start the app on Android as a wrapper around the BrickSlammer java application. The bulk of the logic is in the 'BrickSlammerDesktop' package.

About the game:

Brick Slammer is a fresh twist on the Arkanoid/Breakout concept.
Instead of controlling a paddle at the bottom of the screen, here you directly control the movement of the ball. It is possible(and necessary) to curve and arc the ball in mid-air.

The ball can bounce off the walls, the roof and the floor as well.
However, only blue bricks should be smashed and red bricks should be avoided as much as possible.
A maximum of three red bricks can be hit per level(hitting red bricks is unavoidable in some levels). Hitting all the blue bricks will clear the level.

The game has 50 levels of gradually increasing difficulty and has no ads.
Level progress is automatically saved and you can pick up where you left off.

Please email me feedback, bug reports and suggestions at raghavan.a@gmail.com

Created with the Libgdx framework. Some source files related to Animation, GameObjects, have been reused from the superjumper demo source code, and the license notices have been reproduced.