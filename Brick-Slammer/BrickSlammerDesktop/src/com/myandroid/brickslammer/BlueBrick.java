/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

public class BlueBrick extends DynamicGameObject {
	public static float BLUE_BRICK_WIDTH = 80;
	public static float BLUE_BRICK_HEIGHT = 20;
	public static final int BLUE_BRICK_STATE_SOLID = 0;
	public static final int BLUE_BRICK_STATE_CRUMBLING = 1;
	public static final float BLUE_BRICK_CRUMBLE_TIME = 0.1f * 2;

	float stateTime;
	int state;

	public BlueBrick(float x, float y) {
		super(x, y, BLUE_BRICK_WIDTH, BLUE_BRICK_HEIGHT);
	}

	public void update(float deltaTime) {
		stateTime += deltaTime;
	}

	public void crumble() {
		state = BLUE_BRICK_STATE_CRUMBLING;
		stateTime = 0;
	}

}
