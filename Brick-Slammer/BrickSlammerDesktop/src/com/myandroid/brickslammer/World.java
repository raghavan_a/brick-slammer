/*******************************************************************************
* Copyright [2012] [Raghavan Athimoolam]
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class World {
	public interface WorldListener {
		public void bluehit ();
		public void redhit ();


	}

	public static final float WORLD_WIDTH = 320;
	public static final float WORLD_HEIGHT = 440;
	public static final int WORLD_STATE_RUNNING = 0;
	public static final int WORLD_STATE_NEXT_LEVEL = 1;
	public static final int WORLD_STATE_GAME_OVER = 2;
	public static final int WORLD_STATE_GAME_COMPLETE = 3;
	public static int redbrickpos[];
	public static int bluebrickpos[];
	public static final int NUMBER_OF_BRICK_COLUMNS = 4;
	public static int redbrickshit=0;

	public final Ball ball;

	public final List<RedBrick> redbricks;
	public final List<BlueBrick> bluebricks;
	public final WorldListener listener;
	public final Random rand;

	public float heightSoFar;
	public int score;
	public int state;
	public boolean redhitbool;
	public boolean bluehitbool;
	public int levelno;
	public World (WorldListener listener, int[] bluebrickarray, int[] redbrickarray,int levelnumber) {
		this.ball = new Ball(WORLD_WIDTH/2, 60+Ball.BALL_HEIGHT/2);
		this.ball.velocity.y = 400;
		bluebrickpos=bluebrickarray;
		redbrickpos=redbrickarray;
		this.redhitbool=false;
		this.redbricks = new ArrayList<RedBrick>();
		this.bluebricks = new ArrayList<BlueBrick>();
		this.levelno=levelnumber+1;

		this.listener = listener;
		rand = new Random();
		generateLevel();

		this.heightSoFar = 0;
		this.score = 0;
		this.state = WORLD_STATE_RUNNING;
	}

	private void generateLevel () {

		    for (int i:redbrickpos)
		    {
		    	int columnnumber=i%NUMBER_OF_BRICK_COLUMNS;
		    	int rownumber=i/NUMBER_OF_BRICK_COLUMNS;
		    	RedBrick redbrick = new RedBrick((columnnumber*RedBrick.RED_BRICK_WIDTH)+RedBrick.RED_BRICK_WIDTH/2,(WORLD_HEIGHT-((RedBrick.RED_BRICK_HEIGHT/2)+rownumber*(RedBrick.RED_BRICK_HEIGHT))));
				redbricks.add(redbrick);
		    }

		    for (int i:bluebrickpos)
		    {
		    	int columnnumber=i%NUMBER_OF_BRICK_COLUMNS;
		    	int rownumber=i/NUMBER_OF_BRICK_COLUMNS;
				BlueBrick bluebrick = new BlueBrick((columnnumber*BlueBrick.BLUE_BRICK_WIDTH)+BlueBrick.BLUE_BRICK_WIDTH/2,(WORLD_HEIGHT-((BlueBrick.BLUE_BRICK_HEIGHT/2)+rownumber*(BlueBrick.BLUE_BRICK_HEIGHT))));
				bluebricks.add(bluebrick);
		    }
	}

	public void update (float deltaTime, float accelX) {
		if(state!=WORLD_STATE_GAME_OVER)
		{
			redhitbool=false;
		updateBall(deltaTime, accelX);
		updateBlueBricks(deltaTime);
		updateRedBricks(deltaTime);
		checkRedBrickCollisions(deltaTime);

		if(redhitbool==false)
			{
			checkBlueBrickCollisions(deltaTime);
			}
		}
	}
	private void updateBall (float deltaTime, float accelX) {

if(state!=WORLD_STATE_GAME_OVER)
{
		ball.velocity.x = ball.velocity.x+(accelX*4);
		ball.velocity.y = ball.velocity.y-(accelX/10);
}
else
{
	ball.velocity.x = 0;
	ball.velocity.y = 0;
}

		if(ball.velocity.x<-300)
		{
			ball.velocity.x=-300;
		}
		if(ball.velocity.x>300)
		{
			ball.velocity.x=300;
		}

		if(ball.velocity.y<-300)
		{
			ball.velocity.y=-300;
		}
		if(ball.velocity.y>300)
		{
			ball.velocity.y=300;
		}
		ball.update(deltaTime);
	}
	private void checkRedBrickCollisions(float deltaTime)
	{
		float brickrightbottom_x=0;
		float brickrightbottom_y=0;
		float brickrighttop_x=0;
		float brickrighttop_y=0;
		float bricklefttop_x=0;
		float bricklefttop_y=0;
		float brickleftbottom_x=0;
		float brickleftbottom_y=0;
		if((redbrickshit>3)&&(state==WORLD_STATE_RUNNING))
		{
			state=WORLD_STATE_GAME_OVER;
			return;
		}
		for (int i = 0; i < redbricks.size(); i++)
		{
			RedBrick redbrick = redbricks.get(i);
			brickrightbottom_x=redbrick.position.x+RedBrick.RED_BRICK_WIDTH/2;
			brickrightbottom_y=redbrick.position.y-RedBrick.RED_BRICK_HEIGHT/2;
			brickrighttop_x=redbrick.position.x+RedBrick.RED_BRICK_WIDTH/2;
			brickrighttop_y=redbrick.position.y+RedBrick.RED_BRICK_HEIGHT/2;
			bricklefttop_x=redbrick.position.x-RedBrick.RED_BRICK_WIDTH/2;
			bricklefttop_y=redbrick.position.y+RedBrick.RED_BRICK_HEIGHT/2;
			brickleftbottom_x=redbrick.position.x-RedBrick.RED_BRICK_WIDTH/2;
			brickleftbottom_y=redbrick.position.y-RedBrick.RED_BRICK_HEIGHT/2;
				if (OverlapTester.overlapRectangles(ball.bounds, redbrick.bounds))
				{
					redhitbool=true;
					if(redbrick.state==RedBrick.RED_BRICK_STATE_SOLID)
					{
					listener.redhit();
					redbrick.crumble();
					}
				if(redbrickshit<3)
				{
					if((redbrick.position.y>ball.position.y)&&(redbrick.position.x<ball.position.x))//hit in quadrant 4
					{
						if((ball.velocity.x>=0)&&(ball.velocity.y>=0))//hit on right floor
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((ball.velocity.x<=0)&&(ball.velocity.y<=0))//hit on right lowerside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((brickrightbottom_x<ball.position.x)&&(brickrightbottom_y>ball.position.y))//hit in right bottom corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickrightbottom_x>ball.position.x)&&(brickrightbottom_y>ball.position.y))//hit in right floor in (-,+)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in right lowerside in (-,+)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 4 logic

					else if((redbrick.position.y>ball.position.y)&&(redbrick.position.x>ball.position.x))//hit in quadrant 3
					{

						if((ball.velocity.x<=0)&&(ball.velocity.y>=0))//hit on left floor
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((ball.velocity.x>=0)&&(ball.velocity.y<=0))//hit on left lowerside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((brickleftbottom_x>ball.position.x)&&(brickleftbottom_y>ball.position.y))//hit in left bottom corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickleftbottom_x<ball.position.x)&&(brickleftbottom_y>ball.position.y))//hit in left floor in (+,+)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in left lowerside in (+,+)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 3 logic

					else if((redbrick.position.y<ball.position.y)&&(redbrick.position.x>ball.position.x))//hit in quadrant 2
					{

						if((ball.velocity.x<=0)&&(ball.velocity.y<=0))//hit on left roof
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((ball.velocity.x>=0)&&(ball.velocity.y>=0))//hit on left upperside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((bricklefttop_x>ball.position.x)&&(bricklefttop_y<ball.position.y))//hit in left upper corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((bricklefttop_x<ball.position.x)&&(bricklefttop_y<ball.position.y))//hit in left roof in (+,-)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in left lowerside in (+,-)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 2 logic

					else if((redbrick.position.y<ball.position.y)&&(redbrick.position.x<ball.position.x))//hit in quadrant 1
					{

						if((ball.velocity.x<=0)&&(ball.velocity.y>=0))//hit on right upperside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((ball.velocity.x>=0)&&(ball.velocity.y<=0))//hit on right roof
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickrighttop_x<ball.position.x)&&(brickrighttop_y<ball.position.y))//hit in right upper corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickrighttop_x>ball.position.x)&&(brickrighttop_y<ball.position.y))//hit in right roof in (+,-)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in right lowerside in (+,-)
						{
							ball.velocity.x=-ball.velocity.x;
						}
						
					}//end of quadrant 1 logic

					else if((redbrick.position.x>ball.position.x)||(redbrick.position.x<ball.position.x))//hit exactly between quadrants 1 and 4 or 2 and 3
					{
						ball.velocity.x=-ball.velocity.x;
					}

					else if((redbrick.position.y>ball.position.y)||(redbrick.position.y<ball.position.y))//hit exactly between quadrants 3 and 4 or 1 and 2
					{
						ball.velocity.y=-ball.velocity.y;
					}

				}
				else
				{
					redbrickshit++;
					if(state==WORLD_STATE_RUNNING)
					{
					this.state=WORLD_STATE_GAME_OVER;
					}
					ball.crumble();
				    ball.velocity.x=0;
					ball.velocity.y=0;
				}
				redhitbool=true;
			}   //end of overlap condition
		}	//end of for loop
	}//end of checkRedBrickCollisions

	private void checkBlueBrickCollisions(float deltaTime)
	{
		float brickrightbottom_x=0;
		float brickrightbottom_y=0;
		float brickrighttop_x=0;
		float brickrighttop_y=0;
		float bricklefttop_x=0;
		float bricklefttop_y=0;
		float brickleftbottom_x=0;
		float brickleftbottom_y=0;

		if(bluebricks.size()==0)
		{
			if((GameScreen.currentbluearrayindex+1)>=GameScreen.bluearray.length)
			{
				state=WORLD_STATE_GAME_COMPLETE;
			}
			else
			{
				state=WORLD_STATE_NEXT_LEVEL;
				ball.velocity.x=0;
				ball.velocity.y=0;
			}
		}

		for (int i = 0; i < bluebricks.size(); i++)
		{
			BlueBrick bluebrick = bluebricks.get(i);
			brickrightbottom_x=bluebrick.position.x+BlueBrick.BLUE_BRICK_WIDTH/2;
			brickrightbottom_y=bluebrick.position.y-BlueBrick.BLUE_BRICK_HEIGHT/2;
			brickrighttop_x=bluebrick.position.x+BlueBrick.BLUE_BRICK_WIDTH/2;
			brickrighttop_y=bluebrick.position.y+BlueBrick.BLUE_BRICK_HEIGHT/2;
			bricklefttop_x=bluebrick.position.x-BlueBrick.BLUE_BRICK_WIDTH/2;
			bricklefttop_y=bluebrick.position.y+BlueBrick.BLUE_BRICK_HEIGHT/2;
			brickleftbottom_x=bluebrick.position.x-BlueBrick.BLUE_BRICK_WIDTH/2;
			brickleftbottom_y=bluebrick.position.y-BlueBrick.BLUE_BRICK_HEIGHT/2;
				if (OverlapTester.overlapRectangles(ball.bounds, bluebrick.bounds))
				{
					bluehitbool=true;
					if(bluebrick.state==BlueBrick.BLUE_BRICK_STATE_SOLID)
					{
					listener.bluehit();
					bluebrick.crumble();
					}
					if((bluebrick.position.y>ball.position.y)&&(bluebrick.position.x<ball.position.x))//hit in quadrant 4
					{
						if((ball.velocity.x>=0)&&(ball.velocity.y>=0))//hit on right floor
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((ball.velocity.x<=0)&&(ball.velocity.y<=0))//hit on right lowerside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((brickrightbottom_x<ball.position.x)&&(brickrightbottom_y>ball.position.y))//hit in right bottom corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickrightbottom_x>ball.position.x)&&(brickrightbottom_y>ball.position.y))//hit in right floor in (-,+)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in right lowerside in (-,+)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 4 logic

					else if((bluebrick.position.y>ball.position.y)&&(bluebrick.position.x>ball.position.x))//hit in quadrant 3
					{

						if((ball.velocity.x<=0)&&(ball.velocity.y>=0))//hit on left floor
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((ball.velocity.x>=0)&&(ball.velocity.y<=0))//hit on left lowerside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((brickleftbottom_x>ball.position.x)&&(brickleftbottom_y>ball.position.y))//hit in left bottom corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickleftbottom_x<ball.position.x)&&(brickleftbottom_y>ball.position.y))//hit in left floor in (+,+)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in left lowerside in (+,+)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 3 logic

					else if((bluebrick.position.y<ball.position.y)&&(bluebrick.position.x>ball.position.x))//hit in quadrant 2
					{

						if((ball.velocity.x<=0)&&(ball.velocity.y<=0))//hit on left roof
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((ball.velocity.x>=0)&&(ball.velocity.y>=0))//hit on left upperside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((bricklefttop_x>ball.position.x)&&(bricklefttop_y<ball.position.y))//hit in left upper corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((bricklefttop_x<ball.position.x)&&(bricklefttop_y<ball.position.y))//hit in left roof in (+,-)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in left lowerside in (+,-)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 2 logic

					else if((bluebrick.position.y<ball.position.y)&&(bluebrick.position.x<ball.position.x))//hit in quadrant 1
					{

						if((ball.velocity.x<=0)&&(ball.velocity.y>=0))//hit on right upperside
						{
							ball.velocity.x=-ball.velocity.x;
						}
						else if((ball.velocity.x>=0)&&(ball.velocity.y<=0))//hit on right roof
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickrighttop_x<ball.position.x)&&(brickrighttop_y<ball.position.y))//hit in right upper corner
						{
							ball.velocity.x=-ball.velocity.x;
							ball.velocity.y=-ball.velocity.y;
						}
						else if((brickrighttop_x>ball.position.x)&&(brickrighttop_y<ball.position.y))//hit in right roof in (+,-)
						{
							ball.velocity.y=-ball.velocity.y;
						}
						else //hit in right lowerside in (+,-)
						{
							ball.velocity.x=-ball.velocity.x;
						}
					}//end of quadrant 1 logic

					else if((bluebrick.position.x>ball.position.x)||(bluebrick.position.x<ball.position.x))//hit exactly between quadrants 1 and 4 or 2 and 3
					{
						ball.velocity.x=-ball.velocity.x;
					}

					else if((bluebrick.position.y>ball.position.y)||(bluebrick.position.y<ball.position.y))//hit exactly between quadrants 3 and 4 or 1 and 2
					{
						ball.velocity.y=-ball.velocity.y;
					}

				}//end of overlap condition

		}

		if(bluebricks.size()==0)
		{
			if((GameScreen.currentbluearrayindex+1)>=GameScreen.bluearray.length)
			{
				state=WORLD_STATE_GAME_COMPLETE;
			}
			else
			{
				state=WORLD_STATE_NEXT_LEVEL;
				ball.velocity.x=0;
				ball.velocity.y=0;
			}
		}
	}//end of checkBlueBrickCollisions

	private void updateBlueBricks (float deltaTime) {
		for (int i = 0; i < bluebricks.size(); i++)
		{
			BlueBrick bluebrick = bluebricks.get(i);
			bluebrick.update(deltaTime);
			if (bluebrick.state == BlueBrick.BLUE_BRICK_STATE_CRUMBLING && bluebrick.stateTime > BlueBrick.BLUE_BRICK_CRUMBLE_TIME)
			{
				bluebricks.remove(bluebrick);
			}
		}
	}

	private void updateRedBricks (float deltaTime) {
		for (int i = 0; i < redbricks.size(); i++)
		{
			RedBrick redbrick = redbricks.get(i);
			redbrick.update(deltaTime);
			if (redbrick.state == RedBrick.RED_BRICK_STATE_CRUMBLING && redbrick.stateTime > RedBrick.RED_BRICK_CRUMBLE_TIME)
			{
				redbricks.remove(redbrick);
				redbrickshit++;
			}
		}
	}

}
