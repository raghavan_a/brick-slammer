/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.audio.Music;

public class AboutScreen implements Screen {
	Game game;

	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Rectangle mainMenuBounds;
	Rectangle imageCreditsBounds;
	Rectangle soundCreditsBounds;
	Vector3 touchPoint;
	int startinglevel = 0;

	public static int actual_width = 0;
	public static int actual_height = 0;

	private static final int VIRTUAL_WIDTH = 320;
	private static final int VIRTUAL_HEIGHT = 480;
	private static final float ASPECT_RATIO = (float) VIRTUAL_WIDTH
			/ (float) VIRTUAL_HEIGHT;
	private Camera camera;
	private Rectangle viewport;
	private SpriteBatch sb;

	public Stage stage;
	Rectangle glViewport;

	public AboutScreen(Game game) {

		this.game = game;
		mainMenuBounds = new Rectangle(90, 10, 140, 50);
		imageCreditsBounds = new Rectangle(60, 190, 200, 80);
		soundCreditsBounds = new Rectangle(60, 90, 200, 80);
		try {
			if (Assets.soundEnabled) {
				Assets.music.play();
			}
		} catch (Exception e) {

		}

		batcher = new SpriteBatch();
		boolean stretch = false;
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		glViewport = new Rectangle(0, 0, 320, 480);

		touchPoint = new Vector3();
	}

	public void update(float deltaTime) {

		if (Gdx.input.justTouched()) {
			touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			touchPoint.y = actual_height - touchPoint.y;
			if (OverlapTester.pointInRectangle(mainMenuBounds, touchPoint.x,
					touchPoint.y)) {

				try {
					if (Assets.music.isPlaying()) {
						Assets.music.stop();
					}

					Assets.playSound(Assets.bluehit);
					game.setScreen(new MainMenuScreen(game));
					return;
				} catch (Exception e) {
					game.setScreen(new MainMenuScreen(game));
					e.printStackTrace();
				}

			}

			if (OverlapTester.pointInRectangle(imageCreditsBounds,
					touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				game.setScreen(new ImageCreditsScreen(game));
				return;
			}
			if (OverlapTester.pointInRectangle(soundCreditsBounds,
					touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				game.setScreen(new SoundCreditsScreen(game));
				return;
			}

		}
	}

	public void draw(float deltaTime) {

		GLCommon gl = Gdx.graphics.getGLCommon();

		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.draw();

		batcher.begin();
		batcher.draw(Assets.MainMenuScreenRegion, 0, 0, 320, 480);

		Assets.font_black_24.draw(batcher, "about", 110, World.WORLD_HEIGHT);
		Assets.font.draw(batcher, "conceptualized and", 45,
				World.WORLD_HEIGHT - 35);
		Assets.font.draw(batcher, "developed by:", 85, World.WORLD_HEIGHT - 55);
		Assets.font_black_16.draw(batcher, "raghavan athimoolam", 45,
				World.WORLD_HEIGHT - 75);
		Assets.font.draw(batcher, "Email me at:", 85, World.WORLD_HEIGHT - 115);
		Assets.font_arial_black_16.draw(batcher, "raghavan.a@gmail.com", 60,
				World.WORLD_HEIGHT - 135);

		batcher.draw(Assets.ButtonTemplateRegion, 60, 190, 200, 80);
		Assets.font24.draw(batcher, "image", 115, 255);
		Assets.font24.draw(batcher, "credits", 105, 225);

		batcher.draw(Assets.ButtonTemplateRegion, 60, 90, 200, 80);
		Assets.font24.draw(batcher, "sound", 115, 155);
		Assets.font24.draw(batcher, "credits", 105, 125);

		batcher.draw(Assets.ButtonTemplateRegion, 90, 10, 140, 50);
		Assets.font.draw(batcher, "Main Menu", 105, 45);
		batcher.end();

	}

	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}

	@Override
	public void resize(int width, int height) {

		actual_width = (int) width;
		actual_height = (int) height;

		boolean stretch = false;
		stage.setViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		stage.getCamera().position.set(Gdx.graphics.getWidth() / 2,
				Gdx.graphics.getHeight() / 2, 0);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}
}
