/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.audio.Music;

public class MainMenuScreen implements Screen {
	Game game;

	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Rectangle soundBounds;
	Rectangle playBounds;
	Rectangle helpBounds;
	Rectangle exitBounds;
	Rectangle resumeBounds;
	Rectangle aboutBounds;
	Vector3 touchPoint;
	int startinglevel = 0;

	public static int actual_width = 0;
	public static int actual_height = 0;

	private static final int VIRTUAL_WIDTH = 320;
	private static final int VIRTUAL_HEIGHT = 480;
	private static final float ASPECT_RATIO = (float) VIRTUAL_WIDTH
			/ (float) VIRTUAL_HEIGHT;
	private Camera camera;
	private Rectangle viewport;
	private SpriteBatch sb;

	public Stage stage;
	Rectangle glViewport;

	public MainMenuScreen(Game game) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					Gdx.files.local("data/userdata").read()));
			String strline = br.readLine();
			if (strline != null) {
				startinglevel = Integer.parseInt(strline);
			}
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
			startinglevel = 0;
		}

		if (startinglevel >= 1) {
			resumeBounds = new Rectangle(40, 299, 260, 56);
			playBounds = new Rectangle(40, 243, 260, 56);
			helpBounds = new Rectangle(40, 187, 260, 56);
			aboutBounds = new Rectangle(40, 131, 260, 56);
			exitBounds = new Rectangle(40, 75, 260, 56);
		} else {
			resumeBounds = new Rectangle(0, 0, 0, 0);
			playBounds = new Rectangle(30, 243, 260, 56);
			helpBounds = new Rectangle(30, 187, 260, 56);
			aboutBounds = new Rectangle(40, 131, 260, 56);
			exitBounds = new Rectangle(30, 75, 260, 56);

		}
		this.game = game;
		if (Assets.soundEnabled) {
			Assets.music.play();
		}
		batcher = new SpriteBatch();
		boolean stretch = false;
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		glViewport = new Rectangle(0, 0, 320, 480);
		soundBounds = new Rectangle(10, 10, 32, 32);
		touchPoint = new Vector3();
	}

	public void update(float deltaTime) {
		if (Gdx.input.justTouched()) {
			touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			touchPoint.y = actual_height - touchPoint.y;
			if (OverlapTester.pointInRectangle(playBounds, touchPoint.x,
					touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				try {
					if (Assets.soundEnabled) {
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				game.setScreen(new GameScreen(game, 0, 0));
				return;
			}
			if (OverlapTester.pointInRectangle(resumeBounds, touchPoint.x,
					touchPoint.y)) {
				if ((startinglevel > GameScreen.redarray.length)
						|| (startinglevel == 0)) {
					startinglevel = 1;
				}
				startinglevel--;
				Assets.playSound(Assets.bluehit);

				if (startinglevel < 0) {
					startinglevel = 0;
				}

				try {

					if (Assets.soundEnabled) {
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				game.setScreen(new GameScreen(game, startinglevel,
						startinglevel));
				return;
			}
			if (OverlapTester.pointInRectangle(exitBounds, touchPoint.x,
					touchPoint.y)) {
				System.exit(0);
				return;
			}

			if (OverlapTester.pointInRectangle(helpBounds, touchPoint.x,
					touchPoint.y)) {

				Assets.playSound(Assets.bluehit);
				game.setScreen(new HelpScreen1(game));
				return;
			}
			if (OverlapTester.pointInRectangle(aboutBounds, touchPoint.x,
					touchPoint.y)) {

				Assets.playSound(Assets.bluehit);
				game.setScreen(new AboutScreen(game));
				return;
			}
			if (OverlapTester.pointInRectangle(soundBounds, touchPoint.x,
					touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				Assets.soundEnabled = !Assets.soundEnabled;

				try {
					BufferedWriter out = new BufferedWriter(
							new OutputStreamWriter(Gdx.files.local(
									"/data/userdata").write(false)));
					String level = "" + startinglevel + "\n"
							+ Assets.soundEnabled;
					out.write(level);
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (Assets.soundEnabled) {
					Assets.music.play();
				} else {
					Assets.music.pause();
				}
			}

		}
	}

	public void draw(float deltaTime) {
		GLCommon gl = Gdx.graphics.getGLCommon();
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.draw();

		batcher.begin();
		batcher.draw(Assets.MainMenuScreenRegion, 0, 0, 320, 480);
		batcher.draw(Assets.TitleRegion, 20, 310, 280, 140);

		if (startinglevel >= 1) {
			batcher.draw(Assets.MainMenuRegion_Resume, 40, 75, 260, 280);

		} else {
			batcher.draw(Assets.MainMenuRegion, 30, 75, 260, 210);
		}

		if (Assets.soundEnabled) {
			batcher.draw(Assets.SoundOnRegion, 10, 10, 32, 32);
		} else {
			batcher.draw(Assets.SoundOffRegion, 10, 10, 32, 32);
		}

		batcher.end();
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}

	@Override
	public void resize(int width, int height) {
		actual_width = (int) width;
		actual_height = (int) height;

		boolean stretch = false;
		stage.setViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		stage.getCamera().position.set(Gdx.graphics.getWidth() / 2,
				Gdx.graphics.getHeight() / 2, 0);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {

		try {

			if (Assets.soundEnabled) {
				Assets.music.stop();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

		try {

			if (Assets.soundEnabled) {
				Assets.music.play();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void dispose() {

		try {

			if (Assets.soundEnabled) {
				Assets.music.stop();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
