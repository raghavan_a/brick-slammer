/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {
	public static Texture background1;
	public static TextureRegion backgroundRegion1;
	public static ArrayList backgroundarray = new ArrayList();

	public static BitmapFont font;
	public static BitmapFont font24;
	public static BitmapFont font32;
	public static BitmapFont font_black_16;
	public static BitmapFont font_black_24;
	public static BitmapFont font_black_32;
	public static BitmapFont font_arial_black_16;
	public static BitmapFont font_arial_black_8;
	public static BitmapFont font_arial_black_12;

	public static Music music;
	public static Music music1;
	public static Music music2;
	public static Music music3;
	public static Music music4;
	public static Music music5;
	public static Music music6;
	public static Music music7;
	public static Music music8;
	public static ArrayList musicarray = new ArrayList();
	public static TextureRegion Ball;
	public static Texture ballimage;
	public static Texture ballimage1;
	public static Texture ballimage2;
	public static Texture ballimage3;
	public static Texture ballimage4;
	public static Texture ballimage5;
	public static Animation crumblingBall;

	public static TextureRegion MainMenuScreenRegion;
	public static Texture mainmenuscreenimage;
	public static TextureRegion MainMenuRegion;
	public static Texture mainmenuimage;
	public static TextureRegion TitleRegion;
	public static Texture titleimage;
	public static Texture mainmenuimage_resume;
	public static TextureRegion MainMenuRegion_Resume;

	public static Texture pausescreen;
	public static TextureRegion PauseScreenRegion;
	public static Texture buttontemplate;
	public static TextureRegion ButtonTemplateRegion;

	public static TextureRegion RedBrick;
	public static Texture redbrickimage;
	public static Texture redbrickimage1;
	public static Texture redbrickimage2;
	public static Texture redbrickimage3;
	public static Texture redbrickimage4;
	public static Animation crumblingRedBrick;

	public static TextureRegion BlueBrick;
	public static Texture bluebrickimage;
	public static Texture bluebrickimage1;
	public static Texture bluebrickimage2;
	public static Texture bluebrickimage3;
	public static Texture bluebrickimage4;
	public static Animation crumblingBlueBrick;

	public static TextureRegion gameoverregion;
	public static Texture gameoverimage;
	public static TextureRegion nextlevelregion;
	public static Texture nextlevelimage;
	public static Sound bluehit;
	public static Sound redhit;
	// public static TextureRegion MainMenu;
	// public static Texture mainmenuimage;
	public static TextureRegion TopBarRegion;
	public static Texture topbarimage;

	public static TextureRegion PauseButtonRegion;
	public static Texture pausebuttonimage;
	public static TextureRegion LeftArrow;
	public static Texture leftarrowimage;

	public static TextureRegion RightArrow;
	public static Texture rightarrowimage;
	public static Texture background2;
	public static TextureRegion backgroundRegion2;
	public static Texture background3;
	public static TextureRegion backgroundRegion3;
	public static Texture background4;
	public static TextureRegion backgroundRegion4;
	public static Texture background5;
	public static TextureRegion backgroundRegion5;
	public static Texture background6;
	public static TextureRegion backgroundRegion6;
	public static Texture background7;
	public static TextureRegion backgroundRegion7;
	public static Texture background8;
	public static TextureRegion backgroundRegion8;
	public static Texture background9;
	public static TextureRegion backgroundRegion9;
	public static Texture background10;
	public static TextureRegion backgroundRegion10;

	public static Texture soundoff;
	public static TextureRegion SoundOffRegion;
	public static Texture soundon;
	public static TextureRegion SoundOnRegion;
	public static boolean soundEnabled = false;

	public static Texture loadTexture(String file) {
		return new Texture(Gdx.files.internal(file));
	}

	public static void load() {

		mainmenuscreenimage = loadTexture("data/images/mainmenuscreen.png");
		MainMenuScreenRegion = new TextureRegion(mainmenuscreenimage, 0, 0,
				320, 480);

		mainmenuimage_resume = loadTexture("data/images/mainmenu.png");
		MainMenuRegion_Resume = new TextureRegion(mainmenuimage_resume, 0, 0,
				260, 280);

		mainmenuimage = loadTexture("data/images/mainmenu.png");
		MainMenuRegion = new TextureRegion(mainmenuimage, 0, 55, 260, 225);

		titleimage = loadTexture("data/images/title.png");
		TitleRegion = new TextureRegion(titleimage, 0, 0, 280, 140);

		soundon = loadTexture("data/images/soundon.png");
		SoundOnRegion = new TextureRegion(soundon, 0, 0, 64, 64);

		soundoff = loadTexture("data/images/soundoff.png");
		SoundOffRegion = new TextureRegion(soundoff, 0, 0, 64, 64);

		topbarimage = loadTexture("data/images/topbar.png");
		TopBarRegion = new TextureRegion(topbarimage, 0, 0, 320, 40);
		pausebuttonimage = loadTexture("data/images/pause.png");
		PauseButtonRegion = new TextureRegion(pausebuttonimage, 0, 0, 38, 38);

		pausescreen = loadTexture("data/images/pausescreen.png");
		PauseScreenRegion = new TextureRegion(pausescreen, 0, 0, 200, 220);

		buttontemplate = loadTexture("data/images/buttontemplate.png");
		ButtonTemplateRegion = new TextureRegion(buttontemplate, 0, 0, 150, 32);

		background1 = loadTexture("data/backgrounds/background1.png");
		backgroundRegion1 = new TextureRegion(background1, 0, 0, 320, 480);
		background2 = loadTexture("data/backgrounds/background2.png");
		backgroundRegion2 = new TextureRegion(background2, 0, 0, 320, 480);
		background3 = loadTexture("data/backgrounds/background3.png");
		backgroundRegion3 = new TextureRegion(background3, 0, 0, 320, 480);
		background4 = loadTexture("data/backgrounds/background4.png");
		backgroundRegion4 = new TextureRegion(background4, 0, 0, 320, 480);
		background5 = loadTexture("data/backgrounds/background5.png");
		backgroundRegion5 = new TextureRegion(background5, 0, 0, 320, 480);
		background6 = loadTexture("data/backgrounds/background6.png");
		backgroundRegion6 = new TextureRegion(background6, 0, 0, 320, 480);
		background7 = loadTexture("data/backgrounds/background7.png");
		backgroundRegion7 = new TextureRegion(background7, 0, 0, 320, 480);
		background8 = loadTexture("data/backgrounds/background8.png");
		backgroundRegion8 = new TextureRegion(background8, 0, 0, 320, 480);
		background9 = loadTexture("data/backgrounds/background9.png");
		backgroundRegion9 = new TextureRegion(background9, 0, 0, 320, 480);
		background10 = loadTexture("data/backgrounds/background10.png");
		backgroundRegion10 = new TextureRegion(background10, 0, 0, 320, 480);

		backgroundarray.add(backgroundRegion1);
		backgroundarray.add(backgroundRegion2);
		backgroundarray.add(backgroundRegion3);
		backgroundarray.add(backgroundRegion4);
		backgroundarray.add(backgroundRegion5);
		backgroundarray.add(backgroundRegion6);
		backgroundarray.add(backgroundRegion7);
		backgroundarray.add(backgroundRegion8);
		backgroundarray.add(backgroundRegion9);
		backgroundarray.add(backgroundRegion10);

		ballimage = loadTexture("data/images/ball.png");
		Ball = new TextureRegion(ballimage, 0, 0, 32, 32);
		redbrickimage = loadTexture("data/images/redbrick.png");
		RedBrick = new TextureRegion(redbrickimage, 0, 0, 80, 20);
		bluebrickimage = loadTexture("data/images/bluebrick.png");
		BlueBrick = new TextureRegion(bluebrickimage, 0, 0, 80, 20);
		gameoverimage = loadTexture("data/images/gameover.png");
		gameoverregion = new TextureRegion(gameoverimage, 0, 0, 170, 80);
		nextlevelimage = loadTexture("data/images/nextlevel.png");
		nextlevelregion = new TextureRegion(nextlevelimage, 0, 0, 190, 90);

		leftarrowimage = loadTexture("data/images/leftarrow.png");
		LeftArrow = new TextureRegion(leftarrowimage, 0, 0, 160, 60);

		rightarrowimage = loadTexture("data/images/rightarrow.png");
		RightArrow = new TextureRegion(rightarrowimage, 0, 0, 160, 60);

		font = new BitmapFont(Gdx.files.internal("data/fonts/font.16.fnt"),
				Gdx.files.internal("data/fonts/font.16.png"), false);
		font24 = new BitmapFont(Gdx.files.internal("data/fonts/font.24.fnt"),
				Gdx.files.internal("data/fonts/font.24.png"), false);
		font32 = new BitmapFont(Gdx.files.internal("data/fonts/font.32.fnt"),
				Gdx.files.internal("data/fonts/font.32.png"), false);
		font_black_16 = new BitmapFont(
				Gdx.files.internal("data/fonts/font.black.16.fnt"),
				Gdx.files.internal("data/fonts/font.black.16.png"), false);
		font_black_24 = new BitmapFont(
				Gdx.files.internal("data/fonts/font.black.24.fnt"),
				Gdx.files.internal("data/fonts/font.black.24.png"), false);
		font_arial_black_16 = new BitmapFont(
				Gdx.files.internal("data/fonts/font.arial.16.fnt"),
				Gdx.files.internal("data/fonts/font.arial.16.png"), false);
		font_arial_black_8 = new BitmapFont(
				Gdx.files.internal("data/fonts/font.arial.8.fnt"),
				Gdx.files.internal("data/fonts/font.arial.8.png"), false);
		font_arial_black_12 = new BitmapFont(
				Gdx.files.internal("data/fonts/font.arial.12.fnt"),
				Gdx.files.internal("data/fonts/font.arial.12.png"), false);
		bluebrickimage1 = loadTexture("data/images/bluebrick1.png");
		bluebrickimage2 = loadTexture("data/images/bluebrick2.png");
		bluebrickimage3 = loadTexture("data/images/bluebrick3.png");
		bluebrickimage4 = loadTexture("data/images/bluebrick4.png");
		crumblingBlueBrick = new Animation(0.1f, new TextureRegion(
				bluebrickimage1, 0, 0, 80, 20), new TextureRegion(
				bluebrickimage2, 0, 0, 80, 20), new TextureRegion(
				bluebrickimage3, 0, 0, 80, 20), new TextureRegion(
				bluebrickimage4, 0, 0, 80, 20));

		redbrickimage1 = loadTexture("data/images/redbrick1.png");
		redbrickimage2 = loadTexture("data/images/redbrick2.png");
		redbrickimage3 = loadTexture("data/images/redbrick3.png");
		redbrickimage4 = loadTexture("data/images/redbrick4.png");

		crumblingRedBrick = new Animation(0.1f, new TextureRegion(
				redbrickimage1, 0, 0, 80, 20), new TextureRegion(
				redbrickimage2, 0, 0, 80, 20), new TextureRegion(
				redbrickimage3, 0, 0, 80, 20), new TextureRegion(
				redbrickimage4, 0, 0, 80, 20));

		ballimage1 = loadTexture("data/images/ball1.png");
		ballimage2 = loadTexture("data/images/ball2.png");
		ballimage3 = loadTexture("data/images/ball3.png");
		ballimage4 = loadTexture("data/images/ball4.png");
		ballimage5 = loadTexture("data/images/ball5.png");
		crumblingBall = new Animation(0.1f, new TextureRegion(ballimage1, 0, 0,
				32, 32), new TextureRegion(ballimage2, 0, 0, 32, 32),
				new TextureRegion(ballimage3, 0, 0, 32, 32), new TextureRegion(
						ballimage4, 0, 0, 32, 32), new TextureRegion(
						ballimage5, 0, 0, 32, 32));

		music = Gdx.audio.newMusic(Gdx.files.internal("data/sounds/music.mp3"));
		music.setLooping(true);
		music.setVolume(0.1f);

		music1 = Gdx.audio.newMusic(Gdx.files
				.internal("data/sounds/music1.mp3"));
		music1.setLooping(true);
		music1.setVolume(0.1f);
		// music.play();
		music2 = Gdx.audio.newMusic(Gdx.files
				.internal("data/sounds/music2.mp3"));
		music2.setLooping(true);
		music2.setVolume(0.1f);
		music3 = Gdx.audio.newMusic(Gdx.files
				.internal("data/sounds/music3.mp3"));
		music3.setLooping(true);
		music3.setVolume(0.1f);
		music4 = Gdx.audio.newMusic(Gdx.files
				.internal("data/sounds/music4.mp3"));
		music4.setLooping(true);
		music4.setVolume(0.1f);
		music5 = Gdx.audio.newMusic(Gdx.files
				.internal("data/sounds/music5.mp3"));
		music5.setLooping(true);
		music5.setVolume(0.1f);
		music6 = Gdx.audio.newMusic(Gdx.files
				.internal("data/sounds/music6.mp3"));
		music6.setLooping(true);
		music6.setVolume(0.1f);
		if (musicarray.size() == 0) {
			musicarray.add(music1);
			musicarray.add(music2);
			musicarray.add(music3);
			musicarray.add(music4);
			musicarray.add(music5);
			musicarray.add(music6);
		}
		System.out.println("Assets.musicarray.size()=:" + musicarray.size());
		bluehit = Gdx.audio.newSound(Gdx.files
				.internal("data/sounds/bluehit.mp3"));
		redhit = Gdx.audio.newSound(Gdx.files
				.internal("data/sounds/redhit.mp3"));

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					Gdx.files.local("data/userdata").read()));

			String strline = br.readLine();
			while ((strline != null) && (!strline.trim().equals("true"))
					&& (!strline.trim().equals("false"))) {
				strline = br.readLine();
			}
			if (strline.trim().equals("true")) {
				soundEnabled = true;
			} else {
				soundEnabled = false;
			}
			br.close();

		} catch (Exception e) {
			soundEnabled = true;
			try {
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
						Gdx.files.local("/data/userdata").write(false)));
				String level = "0" + "\n" + soundEnabled;
				out.write(level);
				out.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
	}

	public static void playSound(Sound sound) {
		if (soundEnabled) {
			sound.play(0.1f);
		}
	}
}
