/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

public class Ball extends DynamicGameObject {
	public static final int BALL_STATE_SOLID = 0;
	public static final int BALL_STATE_CRUMBLING = 1;
	public static final float BALL_WIDTH = 32;
	public static final float BALL_HEIGHT = 32;

	int state;
	float stateTime;

	public Ball(float x, float y) {
		super(x, y, BALL_WIDTH, BALL_HEIGHT);
		state = BALL_STATE_SOLID;
		stateTime = 0;
	}

	public void update(float deltaTime) {

		velocity.add(deltaTime, deltaTime);
		position.add(velocity.x * deltaTime, velocity.y * deltaTime);
		bounds.x = position.x - bounds.width / 2;
		bounds.y = position.y - bounds.height / 2;

		if (position.y > World.WORLD_HEIGHT - BALL_HEIGHT / 2) {
			position.y = World.WORLD_HEIGHT - BALL_HEIGHT / 2;
			velocity.y = -velocity.y;
		}

		if (position.y < 60 + BALL_HEIGHT / 2) {
			position.y = 60 + BALL_HEIGHT / 2;
			velocity.y = -velocity.y;
		}

		if (position.x > World.WORLD_WIDTH - BALL_WIDTH / 2) {
			position.x = World.WORLD_WIDTH - BALL_WIDTH / 2;
			velocity.x = -velocity.x;
		}

		if (position.x < BALL_WIDTH / 2) {
			position.x = BALL_WIDTH / 2;
			velocity.x = -velocity.x;
		}
		stateTime += deltaTime;
	}

	public void hitfloorroof() {
		velocity.y = -velocity.y;
		stateTime = 0;
	}

	public void hitwalls() {
		velocity.x = -velocity.x;
		stateTime = 0;
	}

	public void crumble() {
		velocity.y = 0;
		velocity.x = 0;
		state = BALL_STATE_CRUMBLING;
		stateTime = 0;
	}
}
