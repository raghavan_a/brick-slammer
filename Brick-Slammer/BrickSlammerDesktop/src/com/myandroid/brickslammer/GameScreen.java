/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.audio.Music;
import com.myandroid.brickslammer.World.WorldListener;

public class GameScreen implements Screen {
	static final int GAME_READY = 0;
	static final int GAME_RUNNING = 1;
	static final int GAME_PAUSED = 2;
	static final int GAME_LEVEL_END = 3;
	static final int GAME_OVER = 4;
	static final int GAME_COMPLETE = 5;
	private Rectangle glViewport;
	public Stage stage;
	Game game;
	public static int actual_width = 0;
	public static int actual_height = 0;
	int state;
	OrthographicCamera guiCam;
	Vector3 touchPoint;
	SpriteBatch batcher;
	World world;
	WorldListener worldListener;
	WorldRenderer renderer;
	Rectangle pauseBounds;
	Rectangle resumeBounds;
	Rectangle mainMenuBounds;
	Rectangle quitBounds;
	Rectangle leftBounds;
	Rectangle rightBounds;
	int lastScore;
	String scoreString;
	public static int[][] redarray = new int[][] {
			{ 10, 16, 19, 22 },
			{ 10, 19, 22, 27 },
			{ 12, 14, 16, 19, 22 },
			{ 13, 16, 18, 22, 23 },
			{ 11, 13, 16, 17, 18, 20 },
			{ 14, 15, 17, 20, 21 },
			{ 8, 11, 15, 17 },
			{ 9, 10, 11, 14, 18 },
			{ 10, 15, 17, 18 },
			{ 9, 10, 14, 15 },
			{ 5, 6, 9, 14 },
			{ 6, 10, 12, 13, 15 },
			{ 4, 8, 12, 13 },
			{ 4, 8, 9, 11, 14 },
			{ 2, 4, 7, 9, 10 },
			{ 5, 6, 8, 10, 14 },
			{ 7, 9, 12, 14, 15 },
			{ 8, 11, 13, 18, 21 },
			{ 8, 11, 12, 18 },
			{ 11, 12, 15, 16, 17 },
			{ 13, 15, 16, 19, 20, 24 },
			{ 12, 13, 14, 16, 18, 21, 22 },
			{ 5, 7, 11, 14, 15, 17 },
			{ 12, 14, 16, 18, 19, 24 },
			{ 13, 16, 17, 19, 21 },
			{ 9, 12, 14, 17, 18, 19, 22 },
			{ 9, 10, 12, 13, 14, 15, 17, 18, 22 },
			{ 13, 15, 17, 19, 20, 21, 22, 23 },
			{ 9, 10, 12, 14, 15, 16, 17, 18, 20, 22, 23, 24, 28 },
			{ 2, 3, 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 20, 21 },
			{ 12, 13, 14, 15, 18, 19, 20, 21, 23, 31 },
			{ 12, 14, 15, 18, 19, 22, 30, 31 },
			{ 12, 13, 16, 17, 18, 19, 21, 23, 26, 27 },
			{ 4, 5, 7, 8, 9, 10, 12, 14, 16, 17, 20 },
			{ 13, 16, 17, 21, 22, 23, 25, 27 },
			{ 1, 8, 9, 10, 11, 15, 17, 18, 19, 20, 31 },
			{ 8, 9, 11, 12, 13, 14, 15, 17, 19, 22 },
			{ 4, 5, 6, 8, 9, 10, 12, 14, 15, 18, 19, 21, 28 },
			{ 8, 11, 12, 13, 15, 16, 17, 19, 20, 27 },
			{ 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 17, 18, 19, 26, 28, 32 },
			{ 8, 9, 10, 11, 13, 14, 16, 17, 18, 19, 20, 21 },
			{ 0, 1, 4, 5, 6, 8, 10, 12, 13, 14, 15, 17, 18, 19, 20, 32 },
			{ 6, 7, 10, 11, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 30 },
			{ 4, 5, 6, 7, 9, 11, 12, 14, 15, 19, 23, 25 },
			{ 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 21, 22, 24, 25, 26, 35 },
			{ 1, 2, 3, 6, 8, 9, 11, 12, 13, 15, 20, 22, 24, 25, 27, 30 },
			{ 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17, 18, 23, 31 },
			{ 10, 11, 12, 13, 15, 17, 21, 22, 23, 27 },
			{ 4, 5, 6, 7, 8, 9, 10, 11, 13, 16, 17, 18, 20 },
			{ 0, 3, 5, 6, 7, 8, 9, 10, 16, 17 } };

	public static int[][] bluearray = new int[][] { { 21, 27 }, { 13, 16, 21 },
			{ 20, 21, 23 }, { 17, 20, 21 }, { 15, 19, 21 }, { 18, 19, 22 },
			{ 13, 14, 16, 18 }, { 12, 13, 15, 16 }, { 13, 16, 21 },
			{ 8, 11, 13 }, { 4, 8, 10, 11, 15 }, { 8, 11, 14, 19 },
			{ 9, 11, 14, 15 }, { 7, 10, 12, 13 }, { 6, 8, 11 },
			{ 4, 9, 11, 15 }, { 11, 13, 16, 17 }, { 12, 15, 16 },
			{ 10, 16, 19 }, { 13, 14, 18, 19 }, { 14, 17, 18 }, { 15, 17, 19 },
			{ 9, 10, 13, 16 }, { 9, 11, 15, 17, 20 }, { 12, 14, 15, 18, 23 },
			{ 8, 11, 13, 15, 16, 23, 26 }, { 7, 8, 11 }, { 10, 12, 14, 16 },
			{ 13, 19, 21 }, { 7, 16 }, { 16, 17, 27 }, { 9, 27 },
			{ 14, 20, 22 }, { 6, 11, 13 }, { 12, 18 }, { 6, 13 },
			{ 4, 18, 21 }, { 11, 13, 17, 22 }, { 6, 9, 23 }, { 13, 15, 22 },
			{ 12, 15, 22 }, { 9, 22 }, { 15, 26, 32 }, { 8, 10, 27 },
			{ 15, 20, 30 }, { 10, 18, 26 }, { 10, 27 }, { 14, 16, 19 },
			{ 12, 14, 15 }, { 4, 11, 12, 13, 14, 18 } };

	public static int currentbluearrayindex;
	public static int currentredarrayindex;
	public Music music;
	public static ArrayList localmusicarray = new ArrayList();

	public GameScreen(Game game, int bluearrayindex, int redarrayindex) {
		this.game = game;
		currentbluearrayindex = bluearrayindex;
		currentredarrayindex = redarrayindex;
		state = GAME_READY;

		try {
			if (Assets.soundEnabled) {
				if (localmusicarray.size() == 0) {
					localmusicarray.add(Assets.music1);
					localmusicarray.add(Assets.music2);
					localmusicarray.add(Assets.music3);
					localmusicarray.add(Assets.music4);
					localmusicarray.add(Assets.music5);
					localmusicarray.add(Assets.music6);
				}
				music = (Music) localmusicarray.get(bluearrayindex % 6);
				music.play();
			} else {
				music.pause();
			}
		} catch (Exception e) {
		}
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
					Gdx.files.local("data/userdata").write(false)));

			String level = "" + (bluearrayindex + 1) + "\n"
					+ Assets.soundEnabled;
			out.write(level);
			out.close();
		} catch (Exception e) {
		}

		boolean stretch = false;
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		glViewport = new Rectangle(0, 0, 320, 480);

		touchPoint = new Vector3();
		batcher = new SpriteBatch();
		leftBounds = new Rectangle(0, 0, 160, 60);
		rightBounds = new Rectangle(160, 0, 160, 60);
		worldListener = new WorldListener() {
			@Override
			public void bluehit() {
				Assets.playSound(Assets.bluehit);
			}

			@Override
			public void redhit() {
				Assets.playSound(Assets.redhit);
			}

		};
		world = new World(worldListener, bluearray[bluearrayindex],
				redarray[redarrayindex], currentbluearrayindex);
		world.redbrickshit = 0;
		renderer = new WorldRenderer(batcher, world);
		pauseBounds = new Rectangle(280, 445, 35, 38);
		resumeBounds = new Rectangle((world.WORLD_WIDTH / 2 - 280 / 2) + 20,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 160, 240, 40);
		mainMenuBounds = new Rectangle((world.WORLD_WIDTH / 2 - 280 / 2) + 20,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 97, 240, 40);
		quitBounds = new Rectangle((world.WORLD_WIDTH / 2 - 280 / 2) + 20,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 34, 240, 40);
		lastScore = 0;
		scoreString = "SCORE: 0";
	}

	public void update(float deltaTime) {
		switch (state) {
		case GAME_READY:
			updateReady();
			break;
		case GAME_RUNNING:
			updateRunning(deltaTime);
			break;
		case GAME_PAUSED:
			updatePaused();
			break;
		case GAME_COMPLETE:
			updateGameComplete();
			break;
		case GAME_LEVEL_END:
			updateLevelEnd();
			break;
		case GAME_OVER:
			updateGameOver();
			break;

		}

	}

	private void updateReady() {
		if (Gdx.input.justTouched()) {
			state = GAME_RUNNING;
		}
	}

	private void updateRunning(float deltaTime) {
		float accel = 0;
		if (Gdx.input.justTouched()) {
			touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			touchPoint.y = actual_height - touchPoint.y;
			if (OverlapTester.pointInRectangle(pauseBounds, touchPoint.x,
					touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				state = GAME_PAUSED;
				return;
			}
		}

		if (Gdx.app.getType() == Application.ApplicationType.Android) {
		}
		if (Gdx.input.isTouched()) {
			touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			touchPoint.y = actual_height - touchPoint.y;
			if (OverlapTester.pointInRectangle(leftBounds, touchPoint.x,
					touchPoint.y)) {
				accel = -2;
			}
			if (OverlapTester.pointInRectangle(rightBounds, touchPoint.x,
					touchPoint.y)) {
				accel = 2;
			}

		}

		if (Gdx.input.isKeyPressed(Keys.DPAD_LEFT))
			accel = -5;
		if (Gdx.input.isKeyPressed(Keys.DPAD_RIGHT))
			accel = 5;

		if (world.state == World.WORLD_STATE_RUNNING) {
			world.update(deltaTime, accel);
		}

		if (world.state == World.WORLD_STATE_GAME_OVER) {
			state = GAME_OVER;
		}
		if (world.state == World.WORLD_STATE_NEXT_LEVEL) {
			state = GAME_LEVEL_END;
		}
		if (world.state == World.WORLD_STATE_GAME_COMPLETE) {
			state = GAME_COMPLETE;
		}
	}

	private void updatePaused() {
		if (Gdx.input.justTouched()) {
			renderer.render();
			batcher.enableBlending();
			batcher.begin();
			batcher.end();
			touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			touchPoint.y = actual_height - touchPoint.y;
			if (OverlapTester.pointInRectangle(resumeBounds, touchPoint.x,
					touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				state = GAME_RUNNING;
				return;
			}
			if (OverlapTester.pointInRectangle(mainMenuBounds, touchPoint.x,
					touchPoint.y)) {
				try {
					// System.out.println("mainMenuBounds");
					Assets.playSound(Assets.bluehit);
					// state = GAME_RUNNING;
					game.setScreen(new MainMenuScreen(game));
					return;
				} catch (Exception e) {
					game.setScreen(new MainMenuScreen(game));
					e.printStackTrace();
				}

			}
			if (OverlapTester.pointInRectangle(quitBounds, touchPoint.x,
					touchPoint.y)) {
				try {

					// System.out.println("quitBounds");
					// if(Assets.soundEnabled)
					// {
					Assets.playSound(Assets.bluehit);
					// music.stop();
					// music.dispose();
					// }

					// Gdx.app.exit();
					System.exit(0);
					// state = GAME_RUNNING;
					return;

				} catch (Exception e) {
					Gdx.app.exit();
					e.printStackTrace();
				}
			}

			// state = GAME_RUNNING;

			/*
			 * //guiCam.unproject(touchPoint.set(Gdx.input.getX(),
			 * Gdx.input.getY(), 0)); touchPoint.set(Gdx.input.getX(),
			 * Gdx.input.getY(), 0); if
			 * (OverlapTester.pointInRectangle(resumeBounds, touchPoint.x,
			 * touchPoint.y)) { Assets.playSound(Assets.clickSound); state =
			 * GAME_RUNNING; return; }
			 * 
			 * if (OverlapTester.pointInRectangle(quitBounds, touchPoint.x,
			 * touchPoint.y)) { Assets.playSound(Assets.clickSound);
			 * game.setScreen(new MainMenuScreen(game)); return; }
			 */

		}
	}

	private void updateLevelEnd() {

		try {

			if (Gdx.input.justTouched()) {

				// System.out.println("currentbluearrayindex"+currentbluearrayindex);
				// System.out.println("bluearray.length"+bluearray.length);

				if ((currentbluearrayindex + 1) >= bluearray.length) {
					// String
					// gamecomplete="You have completed the entire game!";
					// Assets.font.draw(batcher, gamecomplete, 10, 200);
					state = GAME_COMPLETE;

				} else {
					if (music.isPlaying()) {
						music.stop();
						// music.dispose();
					}
					game.setScreen(new GameScreen(game,
							++currentbluearrayindex, ++currentredarrayindex));
				}

			}
		} catch (Exception e) {
			// state = GAME_COMPLETE;
			game.setScreen(new GameScreen(game, ++currentbluearrayindex,
					++currentredarrayindex));
			e.printStackTrace();
		}

	}

	private void updateGameOver() {

		try {

			if (Gdx.input.justTouched()) {
				world.redbrickshit = 0;
				if (music.isPlaying()) {
					music.stop();
					// music.dispose();
				}
				game.setScreen(new GameScreen(game, currentbluearrayindex,
						currentredarrayindex));
			}

		} catch (Exception e) {
			game.setScreen(new GameScreen(game, currentbluearrayindex,
					currentredarrayindex));
			e.printStackTrace();
		}

	}

	private void updateGameComplete() {

		try {

			if (Gdx.input.justTouched()) {
				world.levelno = 1;
				world.redbrickshit = 0;
				if ((Assets.soundEnabled) && (music.isPlaying())) {
					music.stop();
					// music.dispose();
				}
				// game.setScreen(new
				// GameScreen(game,currentbluearrayindex,currentredarrayindex));
				game.setScreen(new GameScreen(game, 0, 0));
			}

		} catch (Exception e) {
			game.setScreen(new GameScreen(game, 0, 0));
			e.printStackTrace();
		}

	}

	public void draw(float deltaTime) {
		// GLCommon gl = Gdx.gl;
		// gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		GLCommon gl = Gdx.graphics.getGLCommon();

		// Color backgroundColor = Color.BLACK;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.draw();

		renderer.render();

		// guiCam.update();
		// batcher.setProjectionMatrix(guiCam.combined);
		batcher.enableBlending();
		batcher.begin();
		// presentRunning();
		switch (state) {
		case GAME_READY:
			presentReady();
			break;
		case GAME_RUNNING:
			presentRunning();
			break;
		case GAME_PAUSED:
			presentPaused();
			break;
		case GAME_COMPLETE:
			presentGameComplete();
			break;
		case GAME_LEVEL_END:
			presentLevelEnd();
			break;
		case GAME_OVER:
			presentGameOver();
			break;

		}

		batcher.end();

	}

	private void presentReady() {
		// batcher.draw(Assets.ButtonTemplateRegion, world.WORLD_WIDTH/2-150/2,
		// world.WORLD_HEIGHT/2-120/2, 150, 120);
		Assets.font24.draw(batcher, "Touch Screen", 50,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 168);
		Assets.font24.draw(batcher, "To Start", 85,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 138);
	}

	private void presentRunning() {
		String fonttest = "Test Font";
		// batcher.draw(Assets.pause, 320 - 64, 480 - 64, 64, 64);
		// Assets.font.draw(batcher, fonttest, 5, 480);
		// batch.draw(Assets.TopBarRegion, 0, 440, 320, 40);
		// batch.draw(Assets.PauseButtonRegion, 280, 445, 35, 38);
	}

	private void presentPaused() {
		batcher.draw(Assets.PauseScreenRegion, world.WORLD_WIDTH / 2 - 280 / 2,
				world.WORLD_HEIGHT / 2 - 210 / 2, 280, 220);
		batcher.draw(Assets.ButtonTemplateRegion,
				(world.WORLD_WIDTH / 2 - 280 / 2) + 20,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 160, 240, 40);

		batcher.draw(Assets.ButtonTemplateRegion,
				(world.WORLD_WIDTH / 2 - 280 / 2) + 20,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 97, 240, 40);

		batcher.draw(Assets.ButtonTemplateRegion,
				(world.WORLD_WIDTH / 2 - 280 / 2) + 20,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 34, 240, 40);

		Assets.font24.draw(batcher, "Resume Game",
				(world.WORLD_WIDTH / 2 - 280 / 2) + 30,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 188);
		Assets.font24.draw(batcher, "Main Menu",
				(world.WORLD_WIDTH / 2 - 280 / 2) + 55,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 125);
		Assets.font24.draw(batcher, "Exit Game",
				(world.WORLD_WIDTH / 2 - 280 / 2) + 55,
				(world.WORLD_HEIGHT / 2 - 210 / 2) + 62);

	}

	private void presentLevelEnd() {
		// String topText = "the princess is ...";
		// String bottomText = "in another castle!";
		// float topWidth = Assets.font.getBounds(topText).width;
		// float bottomWidth = Assets.font.getBounds(bottomText).width;
		// Assets.font.draw(batcher, topText, 160 - topWidth / 2, 480 - 40);
		// Assets.font.draw(batcher, bottomText, 160 - bottomWidth / 2, 40);
		// batcher.draw(Assets.nextlevelregion, 160 - 160 / 2, 240 - 96 / 2,
		// 190, 90);
		Assets.font24.draw(batcher, "Level Cleared!", 45,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 168);
		Assets.font24.draw(batcher, "Touch Screen", 50,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 128);
		Assets.font24.draw(batcher, "To Continue!", 60,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 108);

	}

	private void presentGameOver() {
		Assets.font24.draw(batcher, "Game Over!", 70,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 168);
		Assets.font24.draw(batcher, "Touch Screen", 50,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 128);
		Assets.font24.draw(batcher, "To Try Again!", 50,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 100);
		// batcher.draw(Assets.gameoverregion, 160 - 160 / 2, 240 - 96 / 2, 170,
		// 80);
		// float scoreWidth = Assets.font.getBounds(scoreString).width;
		// Assets.font.draw(batcher, scoreString, 160 - scoreWidth / 2, 480 -
		// 20);
	}

	private void presentGameComplete() {
		// String gamecomplete="You have completed";
		// String gamecomplete1="the entire game!";
		// Assets.font.draw(batcher, gamecomplete, 10, 200);
		// Assets.font.draw(batcher, gamecomplete1, 10, 150);
		Assets.font24.draw(batcher, "Congratulations!", 25,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 188);
		Assets.font24.draw(batcher, "You have", 90,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 148);
		Assets.font24.draw(batcher, "cleared all levels!", 5,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 120);
		Assets.font24.draw(batcher, "Touch Screen to", 40,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 80);
		Assets.font24.draw(batcher, "restart the Game!", 5,
				(world.WORLD_HEIGHT / 2 - 220 / 2) + 58);
		// float scoreWidth = Assets.font.getBounds(scoreString).width;
		// Assets.font.draw(batcher, scoreString, 160 - scoreWidth / 2, 480 -
		// 20);
	}

	@Override
	public void render(float delta) {
		if (world.state != GAME_OVER) {
			update(delta);
			draw(delta);
		}

	}

	@Override
	public void resize(int width, int height) {

		actual_width = (int) width;
		actual_height = (int) height;

		boolean stretch = false;
		stage.setViewport(320, 480, true);
		stage.getCamera().position.set(320 / 2, 480 / 2, 0);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {

		try {

			if (music.isPlaying()) {
				music.pause();
				// music.dispose();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void pause() {
		if (state == GAME_RUNNING)
			state = GAME_PAUSED;
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

		try {

			if (Assets.soundEnabled) {
				music.pause();
				// music.dispose();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
