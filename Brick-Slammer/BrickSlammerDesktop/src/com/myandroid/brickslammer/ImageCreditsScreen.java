/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.audio.Music;

public class ImageCreditsScreen implements Screen {
	Game game;

	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Rectangle BackBounds;
	Rectangle imageCreditsBounds;
	Rectangle soundCreditsBounds;
	Vector3 touchPoint;
	int startinglevel = 0;

	public static int actual_width = 0;
	public static int actual_height = 0;

	private static final int VIRTUAL_WIDTH = 320;
	private static final int VIRTUAL_HEIGHT = 480;
	private static final float ASPECT_RATIO = (float) VIRTUAL_WIDTH
			/ (float) VIRTUAL_HEIGHT;
	private Camera camera;
	private Rectangle viewport;
	private SpriteBatch sb;

	public Stage stage;
	Rectangle glViewport;

	public ImageCreditsScreen(Game game) {

		this.game = game;
		BackBounds = new Rectangle(90, 10, 140, 50);
		try {
			if (Assets.soundEnabled) {
				Assets.music.play();
			}
		} catch (Exception e) {
		}
		batcher = new SpriteBatch();
		boolean stretch = false;
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		glViewport = new Rectangle(0, 0, 320, 480);

		touchPoint = new Vector3();
	}

	public void update(float deltaTime) {

		if (Gdx.input.justTouched()) {
			touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			touchPoint.y = actual_height - touchPoint.y;
			if (OverlapTester.pointInRectangle(BackBounds, touchPoint.x,
					touchPoint.y)) {
				Assets.playSound(Assets.bluehit);
				game.setScreen(new AboutScreen(game));
				return;
			}
		}
	}

	public void draw(float deltaTime) {
		GLCommon gl = Gdx.graphics.getGLCommon();
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.draw();

		batcher.begin();
		batcher.draw(Assets.MainMenuScreenRegion, 0, 0, 320, 480);

		Assets.font_arial_black_12.draw(batcher,
				"All images are from FreeDigitalPhotos.net", 5,
				World.WORLD_HEIGHT + 20);
		Assets.font_arial_black_12.draw(batcher,
				"and their creators are as follows:", 5, World.WORLD_HEIGHT);

		Assets.font_arial_black_12.draw(batcher,
				"Digital Background by dream designs.", 5,
				World.WORLD_HEIGHT - 50);
		Assets.font_arial_black_12
				.draw(batcher, "Grunge Background by happykanppy.", 5,
						World.WORLD_HEIGHT - 70);
		Assets.font_arial_black_12.draw(batcher, "Stainless Steel Backdrop ",
				5, World.WORLD_HEIGHT - 90);
		Assets.font_arial_black_12.draw(batcher, "by oana roxana birtea.", 25,
				World.WORLD_HEIGHT - 110);
		Assets.font_arial_black_12.draw(batcher,
				"Minimal Background by Pixomar.", 5, World.WORLD_HEIGHT - 130);
		Assets.font_arial_black_12.draw(batcher,
				"Cloud And Sky On Grunge Paper", 5, World.WORLD_HEIGHT - 150);
		Assets.font_arial_black_12.draw(batcher, "by MR LIGHTMAN.", 25,
				World.WORLD_HEIGHT - 170);
		Assets.font_arial_black_12.draw(batcher,
				"Wave And Dots Background by Michal Marcol.", 5,
				World.WORLD_HEIGHT - 190);
		Assets.font_arial_black_12.draw(batcher,
				"White Brick Room by scottchan.", 5, World.WORLD_HEIGHT - 210);
		Assets.font_arial_black_12.draw(batcher, "Waves And Growth Background",
				5, World.WORLD_HEIGHT - 230);
		Assets.font_arial_black_12.draw(batcher, "by Michal Marcol.", 25,
				World.WORLD_HEIGHT - 250);
		Assets.font_arial_black_12
				.draw(batcher, "Celebrate New Year by scottchan.", 5,
						World.WORLD_HEIGHT - 270);
		Assets.font_arial_black_12.draw(batcher,
				"Retro Abstract Background by digital art.", 5,
				World.WORLD_HEIGHT - 290);
		Assets.font_arial_black_12.draw(batcher, "Tre Pulsanti by Idea go.", 5,
				World.WORLD_HEIGHT - 310);
		Assets.font_arial_black_12.draw(batcher, "Pointer Light by Idea go.",
				5, World.WORLD_HEIGHT - 330);
		batcher.draw(Assets.ButtonTemplateRegion, 90, 10, 140, 50);
		Assets.font.draw(batcher, "Back", 130, 40);
		batcher.end();

	}

	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}

	@Override
	public void resize(int width, int height) {

		actual_width = (int) width;
		actual_height = (int) height;
		boolean stretch = false;
		stage.setViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
				true);
		stage.getCamera().position.set(Gdx.graphics.getWidth() / 2,
				Gdx.graphics.getHeight() / 2, 0);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
	}
}
