/*******************************************************************************
 * Copyright [2012] [Raghavan Athimoolam]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.myandroid.brickslammer;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class WorldRenderer {
	static final float FRUSTUM_WIDTH = 10;
	static final float FRUSTUM_HEIGHT = 15;
	World world;
	SpriteBatch batch;
	Texture backgroundimage;
	TextureRegion background;

	public WorldRenderer(SpriteBatch batch, World world) {
		this.world = world;
		this.batch = batch;
	}

	public void render() {
		renderBackground();
		renderObjects();
	}

	public void renderBackground() {
		String levelno = "Level-" + world.levelno;
		String redbricks = "Red Hits-" + world.redbrickshit;
		batch.begin();
		batch.draw((TextureRegion) Assets.backgroundarray
				.get((world.levelno - 1) % 10), 0, 0, 320, 480);
		batch.draw(Assets.LeftArrow, 0, 0, 160, 60);
		batch.draw(Assets.RightArrow, 160, 0, 160, 60);
		batch.draw(Assets.TopBarRegion, 0, 440, 320, 40);
		batch.draw(Assets.PauseButtonRegion, 270, 443, 35, 38);
		Assets.font.draw(batch, levelno, 10, 470);
		Assets.font.draw(batch, redbricks, 120, 470);
		batch.end();
	}

	public void renderObjects() {
		batch.enableBlending();
		batch.begin();
		renderBall();
		renderRedBricks();
		renderBlueBricks();
		batch.end();
	}

	private void renderBall() {
		TextureRegion keyFrame = Assets.Ball;
		if (world.ball.state == Ball.BALL_STATE_CRUMBLING) {
			keyFrame = Assets.crumblingBall.getKeyFrame(world.ball.stateTime,
					Animation.ANIMATION_NONLOOPING);
		}
		batch.draw(keyFrame, world.ball.position.x - Ball.BALL_WIDTH / 2,
				world.ball.position.y - Ball.BALL_HEIGHT / 2, 32, 32);
	}

	private void renderRedBricks() {
		int len = world.redbricks.size();
		for (int i = 0; i < len; i++) {
			RedBrick redbrick = world.redbricks.get(i);
			TextureRegion keyFrame = Assets.RedBrick;
			if (redbrick.state == RedBrick.RED_BRICK_STATE_CRUMBLING) {
				keyFrame = Assets.crumblingRedBrick.getKeyFrame(
						redbrick.stateTime, Animation.ANIMATION_NONLOOPING);
			}
			batch.draw(keyFrame, redbrick.position.x - RedBrick.RED_BRICK_WIDTH
					/ 2, redbrick.position.y - RedBrick.RED_BRICK_HEIGHT / 2,
					80, 20);
		}
	}

	private void renderBlueBricks() {
		int len = world.bluebricks.size();
		for (int i = 0; i < len; i++) {
			BlueBrick bluebrick = world.bluebricks.get(i);
			TextureRegion keyFrame = Assets.BlueBrick;
			if (bluebrick.state == BlueBrick.BLUE_BRICK_STATE_CRUMBLING) {
				keyFrame = Assets.crumblingBlueBrick.getKeyFrame(
						bluebrick.stateTime, Animation.ANIMATION_NONLOOPING);
			}
			batch.draw(keyFrame, bluebrick.position.x
					- BlueBrick.BLUE_BRICK_WIDTH / 2, bluebrick.position.y
					- BlueBrick.BLUE_BRICK_HEIGHT / 2, 80, 20);
		}
	}
}
